package facci.sanchezartegajose.practica6;

import android.app.Activity;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Proximidad extends Activity implements SensorEventListener {

    TextView texto;
    LinearLayout pantalla;
    private Sensor proximidad;
    SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proximidad);


        pantalla=(LinearLayout)findViewById(R.id.pantalla);
        texto =(TextView)findViewById(R.id.textV);
        sensorManager =(SensorManager)getSystemService(SENSOR_SERVICE);
        proximidad =sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        String text = String.valueOf(event.values[0]);
        texto.setText(text);
        float valor =Float.parseFloat(text);
        if(valor==0){
            pantalla.setBackgroundColor(Color.GREEN);
        }else{
            pantalla.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,proximidad,sensorManager.SENSOR_DELAY_NORMAL);

    }
}


package facci.sanchezartegajose.practica6;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
    Button buttonSensores;
    Button buttonsalir;
    Button buttoncamara;
    Button buttonvibracion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonSensores= (Button) findViewById(R.id.btnSensores);
        buttonSensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MainActivity.this, ActividadSensoresAcelerometro.class);
                startActivity(intent);
            } });
   


        buttonvibracion=(Button)findViewById(R.id.button3);
        buttonvibracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  // se genera la navegabilidad entre la actividad principal y la actividad de vibracion
                Intent intent = new Intent(MainActivity.this, Vibracion.class);
                startActivity(intent);
            } });
        buttoncamara=(Button)findViewById(R.id.button2);
        buttoncamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  // se genera la navegabilidad entre la actividad principal y la actividad de vibracion
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(intent);
            } });

        buttonsalir =(Button)findViewById(R.id.button5);
        buttonsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }});

    }
}




package facci.sanchezartegajose.practica6;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActividadSensoresAcelerometro extends Activity {

    Button buttonproximidad;
    Button buttonacelerometro;
    Button  buttonOrientacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensores_acelerometro);


        buttonOrientacion=(Button)findViewById(R.id.buttonbrujula);
        buttonOrientacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadSensoresAcelerometro.this, SensorOrientacion.class);
                startActivity(intent);
            } });


        buttonproximidad=(Button)findViewById(R.id.buttonproximidad);
        buttonproximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadSensoresAcelerometro.this, Proximidad.class);
                startActivity(intent);
            } });
        buttonacelerometro=(Button)findViewById(R.id.buttonacelerometro);
        buttonacelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActividadSensoresAcelerometro.this, SensorAcelerometro.class);
                startActivity(intent);
            } });
    }
    }




